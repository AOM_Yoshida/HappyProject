//
//  EastRoomLayer.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#include "EastRoomLayer.h"
#include "SouthRoomScene.h"
#include "NorthRoomScene.h"
#include "SceneUtils.h"
#include "ImageUtils.h"

void EastRoomLayer::xtSwipeGesture(XTTouchDirection direction, float distance, float speed)
{
    CCLOG("test");
    
    switch (direction) {
        case XTLayer::LEFT:
        {
            // 遷移先の画面のインスタンス
            Scene *pScene = NorthRoomScene::createScene();
            
            SceneUtils::setSceneLodaer(pScene,SceneUtils::SLIDEIN_L);
           
        }
            break;
            
        case XTLayer::RIGHT:
        {
            // 遷移先の画面のインスタンス
            Scene *pScene = SouthRoomScene::createScene();
            
            SceneUtils::setSceneLodaer(pScene,SceneUtils::SLIDEIN_R);
        }
            break;
    }
}

void EastRoomLayer::setBacground()
{
    
    //背景を設置
    Sprite* background = Sprite::create(EAST_ROOM_BACKGROUND_IMG);
    background->setContentSize(getWindowSize());
    background->setAnchorPoint(Point(0,0));
    this->addChild(background, 0); //第2引数は表示順
}

void EastRoomLayer::setTitle(std::string text)
{
    Point origin = Director::getInstance()->getVisibleOrigin();  //マルチレゾリューション対応がどうとか
    //タイトルを設置
    auto lbl_TitleScene = LabelTTF::create(text, "HiraKakuProN-W6", 30);
    lbl_TitleScene->setPosition(Point(origin.x + getWindowSize().width/2,
                                      origin.y + getWindowSize().height
                                      -lbl_TitleScene->getContentSize().height));
    this->addChild(lbl_TitleScene,1);
}

Size EastRoomLayer::getWindowSize()
{
    return  Director::getInstance()->getVisibleSize();
    
}

