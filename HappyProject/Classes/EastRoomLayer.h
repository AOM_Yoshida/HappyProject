//
//  EastRoomLayer.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#ifndef __HappyProject__EastRoomLayer__
#define __HappyProject__EastRoomLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "XTLayer.h"

USING_NS_CC;

class EastRoomLayer :public XTLayer
{
    
public:
    
    virtual void xtSwipeGesture(XTTouchDirection direction, float distance, float speed);
    
    void setBacground();
    
    void setTitle(std::string text);
    
    Size getWindowSize();
    
};


#endif /* defined(__HappyProject__EastRoomLayer__) */
