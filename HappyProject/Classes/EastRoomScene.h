//
//  EastRoomScene.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#ifndef __HappyProject__EastRoomScene__
#define __HappyProject__EastRoomScene__

#include <stdio.h>
#include "SouthRoomLayer.h"

class EastRoomScene : public SouthRoomLayer
{
public:
    
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(EastRoomScene);
    
    
    
};



#endif /* defined(__HappyProject__EastRoomScene__) */
