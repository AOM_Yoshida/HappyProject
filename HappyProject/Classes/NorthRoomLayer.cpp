//
//  NorthRoomLayer.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/21.
//
//

#include "NorthRoomLayer.h"
#include "SimpleAudioEngine.h"
#include "WestRoomScene.h"
#include "EastRoomScene.h"
#include "SceneUtils.h"
#include "ImageUtils.h"


void NorthRoomLayer::xtSwipeGesture(XTTouchDirection direction, float distance, float speed)
{
    CCLOG("test");
    
    switch (direction) {
        case XTLayer::LEFT:
        {
            // 遷移先の画面のインスタンス
            Scene *pScene = WestRoomScene::createScene();
            
            SceneUtils::setSceneLodaer(pScene,SceneUtils::SLIDEIN_L);

        }
            break;
            
        case XTLayer::RIGHT:
        {
            // 遷移先の画面のインスタンス東
            Scene *pScene = EastRoomScene::createScene();
            
            SceneUtils::setSceneLodaer(pScene,SceneUtils::SLIDEIN_R);

        }
            break;
            
        default:
            break;
    }
}

void NorthRoomLayer::setBacground()
{
    
    //背景を設置
    Sprite* background = Sprite::create(NORTH_ROOM_BACKGROUND_IMG);
    background->setContentSize(getWindowSize());
    background->setAnchorPoint(Point(0,0));
    this->addChild(background, 0); //第2引数は表示順
}

void NorthRoomLayer::setTitle(std::string text)
{
    Point origin = Director::getInstance()->getVisibleOrigin();  //マルチレゾリューション対応がどうとか
    //タイトルを設置
    auto lbl_TitleScene = LabelTTF::create(text, "HiraKakuProN-W6", 30);
    lbl_TitleScene->setPosition(Point(origin.x + getWindowSize().width/2,
                                      origin.y + getWindowSize().height
                                      -lbl_TitleScene->getContentSize().height));
    this->addChild(lbl_TitleScene,1);

    
}

Size NorthRoomLayer::getWindowSize()
{
    return  Director::getInstance()->getVisibleSize();

}





