//
//  MainRoomLayer.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/21.
//
//

#ifndef __HappyProject__MainRoomLayer__
#define __HappyProject__MainRoomLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "XTLayer.h"

USING_NS_CC;

class NorthRoomLayer :public  XTLayer
{

public:
    
    virtual void xtSwipeGesture(XTTouchDirection direction, float distance, float speed);
    
    void setBacground();
    
    void setTitle(std::string text);
    
    Size getWindowSize();


};


#endif /* defined(__HappyProject__MainRoomLayer__) */
