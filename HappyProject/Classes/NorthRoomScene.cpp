//
//  MainRoomScene.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/21.
//
//

#include "NorthRoomScene.h"
//音をならすためにinclude

#include "TopPageScene.h"
#include "NorthRoomLayer.h"

//using namespace cocos2d;の略。cocos2dの名前空間を利用
USING_NS_CC;

Scene *NorthRoomScene::createScene(){
    
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = NorthRoomScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
    
    
}


bool NorthRoomScene::init(){
    
    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //画面の座標関係の詳しい説明はここ http://www.cocos2d-x.org/wiki/Coordinate_System
    Size visibleSize = Director::getInstance()->getVisibleSize(); //画面のサイズを取得
    
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->onTouchBegan = CC_CALLBACK_2(MainRoomLayer::onTouchBegan , this);
    
    //自作したレイヤーを設置
    NorthRoomLayer *layer = new NorthRoomLayer();
    layer->setContentSize(visibleSize);
    layer->setTouchEnabled(true);
    layer->setBacground();
    layer->setTitle("NorthRoomScene");
    
    this->addChild(layer);
    
    return true;
}
