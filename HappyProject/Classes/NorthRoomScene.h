//
//  MainRoomScene.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/21.
//
//

#ifndef __HappyProject__MainRoomScene__
#define __HappyProject__MainRoomScene__

#include <stdio.h>
#include "cocos2d.h"
#include "NorthRoomLayer.h"
#include "XTLayer.h"

USING_NS_CC;

class NorthRoomScene : public NorthRoomLayer
{
public:
    
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(NorthRoomScene);    

};

#endif /* defined(__HappyProject__MainRoomScene__) */
