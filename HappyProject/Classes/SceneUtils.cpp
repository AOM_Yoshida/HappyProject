//
//  SceneUtils.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#include "SceneUtils.h"

const float TRANSITION_SLIDEIN_TIME = 0.4f;

const float TRANSITION_FADE_TIME = 0.5;


void SceneUtils::setSceneLodaer(Scene* pScene, int animSelect)
{
    switch (animSelect) {
        case SLIDEIN_L:
        {
            TransitionSlideInL* transition = TransitionSlideInL::create(TRANSITION_SLIDEIN_TIME, pScene);
            
            //遷移実行
            Director::getInstance()->replaceScene(transition);
        }
            break;
            
        case SLIDEIN_R:
        {
            TransitionSlideInR* transition = TransitionSlideInR::create(TRANSITION_SLIDEIN_TIME, pScene);
            
            //遷移実行
            Director::getInstance()->replaceScene(transition);
        }
            
            break;
            
        default:
            break;
    }
}

void SceneUtils::setSceneLodaer(Scene *pScene)
{
    TransitionFade* transition = TransitionFade::create(TRANSITION_FADE_TIME, pScene);
    
    //遷移実行  遷移時のアニメーション
    Director::getInstance()->replaceScene(transition);
    
}




