//
//  SceneUtils.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#ifndef __HappyProject__SceneUtils__
#define __HappyProject__SceneUtils__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class SceneUtils{
    
public:
    
    static void setSceneLodaer(Scene* pScene , int animSelect);
    
    static void setSceneLodaer(Scene* pScene );

    
public:
    
    static enum SlideIn{
        SLIDEIN_L = 0,
        SLIDEIN_R,
    };

};

#endif /* defined(__HappyProject__SceneUtils__) */
