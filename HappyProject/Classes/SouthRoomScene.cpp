//
//  SouthRoomScene.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#include "SouthRoomScene.h"

#include "SimpleAudioEngine.h"

//using namespace cocos2d;の略。cocos2dの名前空間を利用
USING_NS_CC;

Scene *SouthRoomScene::createScene(){
    
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SouthRoomScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
    
    
}


bool SouthRoomScene::init(){
    
    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //画面の座標関係の詳しい説明はここ http://www.cocos2d-x.org/wiki/Coordinate_System
    Size visibleSize = Director::getInstance()->getVisibleSize(); //画面のサイズを取得
    
    //    auto listener = EventListenerTouchOneByOne::create();
    //    listener->onTouchBegan = CC_CALLBACK_2(MainRoomLayer::onTouchBegan , this);
    
    //自作したレイヤーを設置
    SouthRoomLayer *layer = new SouthRoomLayer();
    layer->setContentSize(visibleSize);
    layer->setTouchEnabled(true);
    layer->setBacground();
    layer->setTitle("SouthRoomScene");
    
    this->addChild(layer);
    
    return true;
}
