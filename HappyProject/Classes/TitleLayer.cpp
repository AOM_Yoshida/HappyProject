//
//  TitleLayer.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/03/03.
//
//

#include "TitleLayer.h"
#include "ImageUtils.h"
#include "TopPageScene.h"
#include "TitleScene.h"
#include "SceneUtils.h"

void TitleLayer::setBacground()
{
    
    //背景を設置
    Sprite* background = Sprite::create(TITLE_BACKGROUND_IMG);
    background->setContentSize(getWindowSize());
    background->setAnchorPoint(Point(0,0));
    this->addChild(background, 0); //第2引数は表示順
}

void TitleLayer::setTitle(std::string text)
{
    Point origin = Director::getInstance()->getVisibleOrigin();  //マルチレゾリューション対応がどうとか
    //タイトルを設置
    auto lbl_TitleScene = LabelTTF::create(text, "HiraKakuProN-W6", 30);
    lbl_TitleScene->setPosition(Point(origin.x + getWindowSize().width/2,
                                      origin.y + getWindowSize().height
                                      -lbl_TitleScene->getContentSize().height));
    this->addChild(lbl_TitleScene,1);
    
    
}

Size TitleLayer::getWindowSize()
{
    return  Director::getInstance()->getVisibleSize();
    
}

void TitleLayer::setStartBtn()
{
    Point origin = Director::getInstance()->getVisibleOrigin();  //マルチレゾリューション対応がどうとか

    //スタートボタンを設置
    auto startButton = MenuItemImage::create(
                                             "CloseNormal.png",  // 通常状態の画像
                                             "CloseSelected.png",  // 押下状態の画像
                                             CC_CALLBACK_1(TitleLayer::pushStart, this)); // 押下時のアクション
    
    startButton->setPosition(Point(origin.x + getWindowSize().width - startButton->getContentSize().width/2 , origin.y + startButton->getContentSize().height/2));
    
    //create menu, it's an autorelease object
    auto menu = Menu::create(startButton, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu, 1);

}

void TitleLayer::pushStart(Object *pSender)
{
    
    // 遷移先の画面のインスタンス
    Scene *pScene = TopPageScene::createScene();
    
    SceneUtils::setSceneLodaer(pScene);
    
        
}
                      
