//
//  TitleLayer.h
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/03/03.
//
//

#ifndef __HappyProject__TitleLayer__
#define __HappyProject__TitleLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "XTLayer.h"

USING_NS_CC;

class TitleLayer :public  XTLayer
{
    
public:
    
    void setBacground();
    void setTitle(std::string text);
    void setStartBtn();
    void pushStart(Object *pSender);
    
    Size getWindowSize();

    
    
};




#endif /* defined(__HappyProject__TitleLayer__) */
