//
//  TitleSceneSceneScene.cpp
//  swipeGameProject
//
//  Created by 吉田 優輝 on 2015/02/11.
//
//

#include "TitleScene.h"

//音をならすためにinclude
#include "SimpleAudioEngine.h"

#include "TopPageScene.h"

#include "TitleLayer.h"

//using namespace cocos2d;の略。cocos2dの名前空間を利用
USING_NS_CC;

Scene *TitleScene::createScene(){
    
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = TitleScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
    
    
}


bool TitleScene::init(){
    
    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //画面の座標関係の詳しい説明はここ http://www.cocos2d-x.org/wiki/Coordinate_System
    Size visibleSize = Director::getInstance()->getVisibleSize(); //画面のサイズを取得
    //自作したレイヤーを設置
    TitleLayer *layer = new TitleLayer();
    layer->setContentSize(visibleSize);
    layer->setTouchEnabled(true);
    layer->setBacground();
    layer->setTitle("NorthRoomScene");
    layer->setStartBtn();
    
    this->addChild(layer);
    
    return true;
}

