//
//  TitleScene.h
//  swipeGameProject
//
//  Created by 吉田 優輝 on 2015/02/11.
//
//

#ifndef __swipeGameProject__TitleScene__
#define __swipeGameProject__TitleScene__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class TitleScene : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(TitleScene);
    
};


#endif /* defined(__swipeGameProject__TitleScene__) */
