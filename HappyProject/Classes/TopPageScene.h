//
//  TopPageScene.h
//  swipeGameProject
//
//  Created by 吉田 優輝 on 2015/02/11.
//
//

#ifndef __swipeGameProject__TopPageScene__
#define __swipeGameProject__TopPageScene__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class TopPageScene : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    //初期化のメソッド
    virtual bool init();
    
    //create()を使えるようにしている。
    CREATE_FUNC(TopPageScene);

    void pushBack(cocos2d::Object *pSender);
};
#endif /* defined(__swipeGameProject__TopPageScene__) */
