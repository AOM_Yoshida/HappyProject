//
//  WestRoomScene.cpp
//  HappyProject
//
//  Created by 吉田 優輝 on 2015/02/28.
//
//

#include "WestRoomScene.h"

#include "SimpleAudioEngine.h"
#include "WestRoomLayer.h"

//using namespace cocos2d;の略。cocos2dの名前空間を利用
USING_NS_CC;

Scene *WestRoomScene::createScene(){
    
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = WestRoomScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
    
    
}


bool WestRoomScene::init(){
    
    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //画面の座標関係の詳しい説明はここ http://www.cocos2d-x.org/wiki/Coordinate_System
    Size visibleSize = Director::getInstance()->getVisibleSize(); //画面のサイズを取得
    
    //    auto listener = EventListenerTouchOneByOne::create();
    //    listener->onTouchBegan = CC_CALLBACK_2(MainRoomLayer::onTouchBegan , this);
    
    //自作したレイヤーを設置
    WestRoomScene *layer = new WestRoomScene();
    layer->setContentSize(visibleSize);
    layer->setTouchEnabled(true);
    layer->setBacground();
    layer->setTitle("WestRoomScene");
    
    this->addChild(layer);
    
    return true;
}



void WestRoomScene::pushStart(Object *pSender)
{
    // 効果音を鳴らす
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("ComedyWhistle.caf");
    
    
    // 遷移先の画面のインスタンス
    Scene *pScene = WestRoomScene::createScene();
    
    // 0.5秒かけてフェードアウトしながら次の画面に遷移します
    //    引数１:フィードの時間
    //    引数２：移動先のシーン
    //    引数３：フィードの色（オプション）
    TransitionFade* transition = TransitionFade::create(0.5f, pScene);
    
    //遷移実行  遷移時のアニメーション　http://study-cocos2d-x.info/scenelayer/55/
    Director::getInstance()->replaceScene(transition);
}